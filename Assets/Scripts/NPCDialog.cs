﻿using UnityEngine;
using System.Collections;

public class NPCDialog : MonoBehaviour {

	public string[] textToShow;
	public string[] afterDisguiseTalk;

	private int counter;

	private string text = "";

	public bool show;

	void Start () {
		counter = 0;
	}

	void Update () {
		if (GlobalVars.isDisguised == false) {
			text = "<color=white><size=25>" + textToShow [counter] + "</size></color>";
		} else {
			text = "<color=white><size=25>" + afterDisguiseTalk [counter] + "</size></color>";
			if(Application.loadedLevelName != "Tailor" && textToShow[0] != "What a strange feline."){
				if(counter == afterDisguiseTalk.Length - 1){
					GlobalVars.wasGivenScoop = true;
				}
			}
		}
	}

	void OnMouseDown(){
		if (GlobalVars.hasReadExpo == true) {
			show = !show;
		}
	}

	private void OnGUI(){

		if (show == true) {
			OpenTextWindow();
		}
	}

	private void OpenTextWindow(){
		GUI.Box (new Rect (0, 0, Screen.width, Screen.height), "");
		Rect r = new Rect (0, 0, Screen.width, Screen.height);
		Texture2D tex = new Texture2D (1, 1);
		tex.SetPixel (1, 1, new Color (0.0f, 0.0f, 0.0f, 0.95f));
		tex.Apply ();
		GUI.DrawTexture (r, tex);
		if (counter % 2 == 0) {
			GUI.Label (GlobalVars.leftText, text);
		} else {
			GUI.Label (GlobalVars.rightText, text);
		}
		if (GlobalVars.isDisguised == false) {
			if (counter < textToShow.Length - 1) {
				if (GUI.Button (GlobalVars.rButton, "Next")) {
					counter = counter + 1;
				}
			} else {
				if (GUI.Button (GlobalVars.rButton, "End")) {
					show = !show;
					counter = 0;
				}
			}
			if(Application.loadedLevelName != "Tailor" && textToShow[0] != "What a strange feline."){
				GlobalVars.needDisguise = true;
			}
		} else {
			if (counter < afterDisguiseTalk.Length - 1) {
				if (GUI.Button (GlobalVars.rButton, "Next")) {
					counter = counter + 1;
				}
			} else {
				if (GUI.Button (GlobalVars.rButton, "End")) {
					show = !show;
					counter = 0;
				}
			}
		}
	}
}
