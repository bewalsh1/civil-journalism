﻿using UnityEngine;
using System.Collections;

public class Journal : MonoBehaviour {

	private bool showJournal;
	private string text;

	// Use this for initialization
	void Start () {
		text = "";
		showJournal = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown(){
		showJournal = !showJournal;
	}

	private void OnGUI(){
		if (showJournal == true) {
			GUI.Box (new Rect (0, 0, Screen.width, Screen.height), "");
			Rect r = new Rect (0, 0, Screen.width, Screen.height);
			Texture2D tex = new Texture2D (1, 1);
			tex.SetPixel (1, 1, new Color (0.984313725f, 0.95294117f, 0.7098039f, 1.0f));
			tex.Apply ();
			GUI.DrawTexture (r, tex);
			GUI.Label (new Rect (Screen.width - 450, 50, 400, 800), text);
			if (GUI.Button (new Rect (50, 50, 400, 40), "Assignment Letter")) {
				text = "John,\nThere's been a ruckus in Baltimore, and I need you to look into it. You should start by going to the local paper, the Baltimore Wecker, and get the story from them.\n\n-George Adam\nVirginia Press, Editor";
				text = "<color=black><size=26>" + text + "</size></color>";
				GUI.Label (new Rect (Screen.width - 450, 50, 400, 800), text);
				GlobalVars.hasReadExpo = true;
			}
			if (GlobalVars.needDisguise == true) {
				if (GUI.Button (new Rect (50, 100, 400, 40), "Uncooperative Townsfolk")) {
					text = "The locals were rather intolerant of my appearance, perhaps I should find a change of attire if I want to get any information out of them.";
					text = "<color=black><size=26>" + text + "</size></color>";
					GUI.Label (new Rect (Screen.width - 450, 50, 400, 800), text);
				}
			}
			if (GlobalVars.isDisguised == true) {
				if (GUI.Button (new Rect (50, 150, 400, 40), "New Duds")) {
					text = "Now that I look a bit less pompous, perhaps that locals won't be so hostile.";
					text = "<color=black><size=26>" + text + "</size></color>";
					GUI.Label (new Rect (Screen.width - 450, 50, 400, 800), text);
				}
			}

			if(GlobalVars.wasGivenScoop == true){
				if (GUI.Button (new Rect (50, 200, 400, 40), "Baltimore Wecker")) {
					text = "My contact has been run out of town. It looks like I'll have to do this investigating the old fashioned way.";
					text = "<color=black><size=26>" + text + "</size></color>";
					GUI.Label (new Rect (Screen.width - 450, 50, 400, 800), text);
				}
				if (GUI.Button (new Rect (50, 250, 400, 40), "Ruckus")) {
					text = "Soliders fired into a crowd of people.\n\n\nAn angry mob chased them out of town";
					text = "<color=black><size=20>" + text + "</size></color>";
					GUI.Label (new Rect (Screen.width - 450, 50, 400, 800), text);
				}
			}

			if (GUI.Button (GlobalVars.rButton, "End")) {
				showJournal = !showJournal;
				text = "";
			}
		}
	}
}