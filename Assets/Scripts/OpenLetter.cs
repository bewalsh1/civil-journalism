﻿using UnityEngine;
using System.Collections;

public class OpenLetter : MonoBehaviour {

	private bool showDialogue;

	// Use this for initialization
	void Start () {
		showDialogue = !GlobalVars.hasReadExpo;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnGUI(){
		if (showDialogue == true) {
			GUI.Box (new Rect (0, 0, Screen.width, Screen.height), "");
			Rect r = new Rect (0, 0, Screen.width, Screen.height);
			Texture2D tex = new Texture2D (1, 1);
			tex.SetPixel (1, 1, new Color (0.0f, 0.0f, 0.0f, 0.95f));
			tex.Apply ();
			GUI.DrawTexture (r, tex);
			string text = "Now that I've arrived, I'd better check my assignment again. The Editor will have my job if I mess this one up.\n\nI've placed the letter in my green journal.";
			text = "<color=white><size=26>" + text + "</size></color>";
			GUI.Label (new Rect (Screen.width / 4 - 175, Screen.height / 2 + 100, Screen.width - 200, 200), text);
			if (GUI.Button (GlobalVars.rButton, "End")) {
				showDialogue = false;
			}
		}
	}
}
