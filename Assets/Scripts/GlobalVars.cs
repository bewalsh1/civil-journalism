﻿using UnityEngine;
using System.Collections;

public class GlobalVars : MonoBehaviour {

	public static bool isDisguised;
	public static bool needDisguise;
	public static bool hasReadExpo;
	public static bool wasGivenScoop;
	private bool showQuit;
	public static Rect rButton;
	public static Rect leftText;
	public static Rect rightText;

	// Use this for initialization
	void Start () {
		showQuit = false;
		needDisguise = false;
		isDisguised = false;
		hasReadExpo = false;
		wasGivenScoop = false;
		rButton = new Rect ((float)(Screen.width * 0.85), (float)(Screen.height * 0.9), 100, 30);
		leftText = new Rect ((float)(Screen.width * 0.05), (float)(Screen.height * 0.55), (float)(Screen.width * .4), (float)(Screen.height / 3));
		rightText = new Rect ((float)(Screen.width * 0.55), (float)(Screen.height * 0.55), (float)(Screen.width * .4), (float)(Screen.height / 3));
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey ("escape")) {
			showQuit = true;
		}
	}

	void OnGUI(){
		if (showQuit == true) {
			GUI.Box (new Rect (0, 0, Screen.width, Screen.height), "");
			Rect r = new Rect (0, 0, Screen.width, Screen.height);
			Texture2D tex = new Texture2D (1, 1);
			tex.SetPixel (1, 1, new Color (0.0f, 0.0f, 0.0f, 1.0f));
			tex.Apply ();
			GUI.DrawTexture (r, tex);
			string text = "Are you sure you want to quit?";
			text = "<color=white><size=26>" + text + "</size></color>";
			GUI.Label (new Rect (Screen.width / 2 - 175, (float)(Screen.height *.35), Screen.width - 200, 200), text);
			if (GUI.Button (new Rect((float)(Screen.width * .5 + 50), (float)(Screen.height * .5), 100, 30), "No")) {
				showQuit = !showQuit;
			}
			if (GUI.Button (new Rect((float)(Screen.width * .5 - 150), (float)(Screen.height * .5), 100, 30), "Yes")) {
				Application.Quit ();
			}
		}
	}

	void Awake(){
		DontDestroyOnLoad (this);
	}
}
