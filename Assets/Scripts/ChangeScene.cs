﻿using UnityEngine;
using System.Collections;

public class ChangeScene : MonoBehaviour {

	public string SceneToGo;
	private bool showRR;
	private bool showNoGo;

	void Start(){
		showRR = false;
		showNoGo = false;
	}

	void OnMouseDown (){
		if (GlobalVars.hasReadExpo == true || SceneToGo == "ProtoStreet1") {
			Application.LoadLevel (SceneToGo);
		} else {
			if(SceneToGo == "Tailor" && GlobalVars.needDisguise == false){
				showNoGo = !showNoGo;
			}
			else{
				showRR = true;
			}
		}
	}

	void OnGUI(){
		if (showRR == true && SceneToGo != "ProtoStreet1") {
			GUI.Box (new Rect (0, 0, Screen.width, Screen.height), "");
			Rect r = new Rect (0, 0, Screen.width, Screen.height);
			Texture2D tex = new Texture2D (1, 1);
			tex.SetPixel (1, 1, new Color (0.0f, 0.0f, 0.0f, 0.95f));
			tex.Apply ();
			GUI.DrawTexture (r, tex);
			string text = "I shouldn't go anywhere before I check that letter, now that I'm here.";
			text = "<color=white><size=26>" + text + "</size></color>";
			GUI.Label (new Rect (Screen.width / 4 - 175, Screen.height / 2 + 100, Screen.width - 100, 200), text);
			if (GUI.Button (GlobalVars.rButton, "End")) {
				showRR = !showRR;
			}
		}
		if (showNoGo == true) {
			GUI.Box (new Rect (0, 0, Screen.width, Screen.height), "");
			Rect r = new Rect (0, 0, Screen.width, Screen.height);
			Texture2D tex = new Texture2D (1, 1);
			tex.SetPixel (1, 1, new Color (0.0f, 0.0f, 0.0f, 0.95f));
			tex.Apply ();
			GUI.DrawTexture (r, tex);
			string text = "Why would I need to go to the tailor? I'm wearing my Sunday best already!";
			text = "<color=white><size=26>" + text + "</size></color>";
			GUI.Label (new Rect (Screen.width / 4 - 175, Screen.height / 2 + 100, Screen.width - 100, 200), text);
			if (GUI.Button (GlobalVars.rButton, "End")) {
				showNoGo = !showNoGo;
			}
		}
	}
}
