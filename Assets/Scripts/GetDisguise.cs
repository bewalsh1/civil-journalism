﻿using UnityEngine;
using System.Collections;

public class GetDisguise : MonoBehaviour {

	private bool show;

	private string text;

	void Start(){
		show = false;
		text = "Now I have a disguise. I should go back and talk to the locals.";
		text = "<color=white><size=32>" + text + "</size></color>";
	}

	void OnMouseDown (){
		if (GlobalVars.needDisguise == true) {
			GlobalVars.isDisguised = true;
			show = !show;
		}
	}

	private void OnGUI(){	
		if (show == true) {
			OpenTextWindow();
		}
	}

	private void OpenTextWindow(){
		GUI.Box (new Rect (0, 0, Screen.width, Screen.height), "");
		Rect r = new Rect (0, 0, Screen.width, Screen.height);
		Texture2D tex = new Texture2D (1, 1);
		tex.SetPixel (1, 1, new Color (0.0f, 0.0f, 0.0f, 0.85f));
		tex.Apply ();
		GUI.DrawTexture (r, tex);
		GUI.Label (new Rect (Screen.width / 4 + 25, Screen.height / 2 - 100, Screen.width / 2, 200), text);
		if (GUI.Button (GlobalVars.rButton, "End")) {
			show = !show;
		}
	}

}
